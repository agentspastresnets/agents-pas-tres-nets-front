import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CardService } from './services/card.service';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DeckComponent } from './deck/deck.component';
import { CommunityComponent } from './community/community.component';
import { GamesComponent } from './games/games.component';
import { StatisticsComponent } from './statistics/statistics.component';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CardComponent } from './deck/card/card.component';
import { CardstatsComponent } from './deck/cardstats/cardstats.component';
import { CardeditComponent } from './deck/cardedit/cardedit.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { LoginService } from './services/login.service';
import { LoginGuard } from './services/login-gard.service';
import { HistoricalComponent } from './games/historical/historical.component';
import { MapComponent } from './games/map/map.component';
import { CurrentComponent } from './games/current/current.component';
import {GameService} from './services/game.service';

const appRoutes: Routes = [
  { path: '',  component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'community', component: CommunityComponent },
  { path: 'games', component: GamesComponent },
  { path: 'statistics', component: StatisticsComponent },
  { path: 'deck', canActivate: [LoginGuard], component: DeckComponent },
  { path: 'account', canActivate: [LoginGuard], component: AccountComponent },
  { path: 'login', component: LoginComponent },
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DeckComponent,
    CommunityComponent,
    GamesComponent,
    StatisticsComponent,
    AccountComponent,
    LoginComponent,
    CardComponent,
    CardstatsComponent,
    CardeditComponent,
    FourOhFourComponent,
    HistoricalComponent,
    MapComponent,
    CurrentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [
    CardService,
    GameService,
    LoginGuard,
    LoginService
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
