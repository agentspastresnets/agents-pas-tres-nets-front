import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../services/game.service';
import {LoginService} from '../services/login.service';
import {User} from '../models/user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  @Input() user: User;
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.user = this.loginService.getUserIdent();
  }

}
