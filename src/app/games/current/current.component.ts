import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from '../../models/game';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.scss']
})
export class CurrentComponent implements OnInit {

  @Input() currentGames: Game[];
  @Output() selectedGame = new EventEmitter();
  constructor() { }
  ngOnInit() {
    this.currentGames = this.currentGames.sort((a: any, b: any) =>
      new Date(a.endDate).getTime() - new Date(b.endDate).getTime()
    );
  }

  onSelectGame(game) {
    this.selectedGame.emit(game);
  }
}
