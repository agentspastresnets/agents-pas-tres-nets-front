import { Component, OnInit } from '@angular/core';
import { GameService } from '../services/game.service';
import { Game } from '../models/game';
import { Subscription } from 'rxjs';
import { User } from '../models/user';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})

export class GamesComponent implements OnInit {

  currentGames: Game[];
  historicalGames: Game[];
  gamesSubscription: Subscription;
  selectedGame: Game;
  users: Map<number, User>;
  private usersurl: string;

  constructor(
     private gameService: GameService,
     private httpClient: HttpClient
  ) {
    this.currentGames = [];
    this.historicalGames = [];
    this.usersurl = 'https://aptn.redissi.xyz/api/users/';
    this.onFetch();
  }

  ngOnInit() {

  }

  onFetch() {
     this.gameService.getGames();
     this.gamesSubscription = this.gameService.gamesSubject.subscribe(
      (games: Game[]) => {
        if (games) {
          this.currentOrHistoricalDispatcher(games);

        }
      }

    );
  }

  currentOrHistoricalDispatcher(games: Game[]) {
    games.forEach((g) => {
      if ( g.gameStatus.includes('WON') ) {
        g.playersId.forEach((p, i) => {
          this.httpClient
            .get <any>(this.usersurl + p)
            .subscribe( (response) => {
              const r = response;
              g.playersId[i] = r;
            });
        });
        this.historicalGames.push(g);
      } else {
        if ( !g.gameStatus.includes('CANCELED')) {
          g.playersId.forEach((p, i) => {
            this.httpClient
              .get <any>(this.usersurl + p)
              .subscribe((response) => {
                const r = response;
                g.playersId[i] = r;
              });
          });
          this.currentGames.push(g);
        }
      }
    });
    // sort the two arrays by dates
    this.historicalGames = this.historicalGames.sort((b: any, a: any) =>
      new Date(a.endDate).getTime() - new Date(b.endDate).getTime()
    );
    this.currentGames = this.currentGames.sort((a: any, b: any) =>
      new Date(a.endDate).getTime() - new Date(b.endDate).getTime()
    );
  }

  changeSelectedGame(game) {
    this.selectedGame = game;
  }
}
