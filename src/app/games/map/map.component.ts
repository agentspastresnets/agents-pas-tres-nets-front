import {Component, Input, OnInit} from '@angular/core';
import * as L from 'leaflet';
import {Game} from '../../models/game';
import {Icon, Marker} from 'leaflet';
import {GameService} from '../../services/game.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private map: any;
  private historicalGames: Game[];
  private currentGames: Game[];
  _selectedGame: Game;
  private markers: Map<number, Marker>;

  @Input()   set selectedGame(game: Game) {
    if (game) {
      const oldSelectedGame = this._selectedGame;
      if (oldSelectedGame) {
        const oldMarker = this.markers.get(oldSelectedGame.id);
        if (this.historicalGames.find(g => g.id === oldSelectedGame.id)) {
          oldMarker.setIcon(this.historicalIcon);
        } else if (this.currentGames.find(g => g.id === oldSelectedGame.id)) {
          oldMarker.setIcon(this.currentIcon);
        }
      }
      this._selectedGame = game;
      this.markers.get(game.id).setIcon(this.selectedIcon);
      this.map.panTo(new L.LatLng(game.latitude, game.longitude));
    }
  }

  private readonly currentIcon: Icon;
  private readonly historicalIcon: Icon;
  private readonly selectedIcon: Icon;

  constructor(private gameService: GameService) {
    this.currentIcon = L.icon({ iconUrl: './assets/icon_game.png' });
    this.historicalIcon = L.icon({ iconUrl: './assets/icon_marker.png' });
    this.selectedIcon = L.icon({ iconUrl: './assets/icon_place_marker.png' });
    this.markers = new Map<number, Marker>();
  }

  ngOnInit() {
    // Déclaration de la carte avec les coordonnées du centre et le niveau de zoom.
    this.map = L.map('aptnmap').setView([45.75, 4.85], 10);
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'Carte des parties'
    }).addTo(this.map);

    this.getGames();
  }

  getGames() {
    this.gameService.getCurrentGames().subscribe(games => {
      this.currentGames = games;
      this.fillCurrentMarkers();
    });
    this.gameService.getHistoricalGames().subscribe(games => {
      this.historicalGames = games;
      this.fillHistoricalMarkers();
    });
  }

  fillCurrentMarkers() {
    this.currentGames.forEach((g) => {
      if ( g !== undefined) {
        const marker = L.marker([g.latitude, g.longitude], {icon: this.currentIcon});
        this.markers.set(g.id, marker);
        marker.bindPopup(g.gameStatus).addTo(this.map);
      }
    });
  }

  fillHistoricalMarkers() {
    this.historicalGames.forEach((g) => {
      if ( g !== undefined) {
        const marker = L.marker([g.latitude, g.longitude], {icon: this.historicalIcon});
        this.markers.set(g.id, marker);
        marker.bindPopup(g.gameStatus).addTo(this.map);
      }
    });
  }
}
