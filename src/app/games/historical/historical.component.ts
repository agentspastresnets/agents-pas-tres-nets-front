import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Game} from '../../models/game';
import {GameService} from '../../services/game.service';

@Component({
  selector: 'app-historical',
  templateUrl: './historical.component.html',
  styleUrls: ['./historical.component.scss']
})
export class HistoricalComponent implements OnInit {

  @Output() selectedGame = new EventEmitter();
  @Input() historicalGames: Game[];
  constructor() { }

  ngOnInit() {

  }

  onSelectGame(game) {
    this.selectedGame.emit(game);
  }
}
