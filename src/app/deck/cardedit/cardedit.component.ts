import { Component, Input, OnInit } from '@angular/core';
import {  FormBuilder } from '@angular/forms';
import { CardService } from '../../services/card.service';
import {Card} from '../../models/card';

@Component({
  selector: 'app-cardedit',
  templateUrl: './cardedit.component.html',
  styleUrls: ['./cardedit.component.scss']
})
export class CardeditComponent implements OnInit {

  @Input() selectedCard: Card;
  @Input() validForm: boolean;
  metiers: boolean;
  cardAdded: boolean;
  cardDeleted: boolean;

  constructor( private formBuilder: FormBuilder,
               private cardService: CardService) {
          this.cardAdded = false;
          this.cardDeleted = false;
          this.validForm = false;
  }

  ngOnInit() {
  }

  onSaveCard(selectedCard) {
    this.cardService.saveCardToServer(selectedCard);
    this.scrollToTop();
    this.cardAdded = true;
    setTimeout(
      () => {
        this.cardAdded = false;
      }, 3000
    );
  }

  onDelCard() {
    this.scrollToTop();
    this.cardService.deleteCard(this.selectedCard)
       .subscribe(
        () => {
          this.cardDeleted = true;
          setTimeout(
            () => {
              this.cardDeleted = false;
            }, 3000
          );
          this.selectedCard = null;
          this.cardService.getCardsFromServer();
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        });
  }

  checkValid(selectedCard) {
    if (selectedCard.name !== null && selectedCard.name !== ''
      &&  selectedCard.picture !== null && selectedCard.picture !== ''
      && selectedCard.metiers[0].name != null && selectedCard.metiers[0].name !== ''
      && selectedCard.metiers[1].name != null && selectedCard.metiers[1].name !== ''
      && selectedCard.metiers[2].name != null && selectedCard.metiers[2].name !== ''
      && selectedCard.metiers[3].name != null && selectedCard.metiers[3].name !== ''
      && selectedCard.metiers[4].name != null && selectedCard.metiers[4].name !== ''
      && selectedCard.metiers[5].name != null && selectedCard.metiers[5].name !== ''
      && selectedCard.metiers[6].name != null && selectedCard.metiers[6].name !== ''
    ) {
      this.validForm = false;
      return this.validForm;
    } else {
      this.validForm = true;
      return this.validForm;
    }
  }

  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 20); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }
}
