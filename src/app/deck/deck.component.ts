import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';
import { Subscription } from 'rxjs';
import { Card } from '../models/card';

@Component({
  selector: 'app-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss']
})
export class DeckComponent implements OnInit {

  cards: Card[];
  cardsSubscription: Subscription;
  selectedCardId: number;
  selectedCard: Card;
  addButton: boolean;

  constructor(private cardService: CardService) {
    this.onFetch();
  }

   ngOnInit() {
     this.cardsSubscription = this.cardService.cardsSubject.subscribe(
       (cards: Card[]) => {
         if (cards) {
           this.cards = cards;
         }
       }
     );
     this.cardService.emitCardSubject();
     this.addButton = true;
   }

  onFetch() {
     this.cardService.getCardsFromServer();
     this.cardService.emitCardSubject();
  }

  onAddCard() {
    this.selectedCard = this.cardService.getNewCard();
    if ( this.cards !== undefined ) {
      this.cards.push(this.selectedCard);
    }
  }

  onSelectedCardId(id: number) {
    this.selectedCardId = id;
    this.selectedCard = new Card();
    this.cards.map((card) => {
      if (card.id === this.selectedCardId) {
        this.selectedCard.id = card.id;
        this.selectedCard.name = card.name;
        this.selectedCard.picture = card.picture;
        this.selectedCard.usage = card.usage;
        this.selectedCard.metiers = card.metiers;
      }
    });
    this.scrollToTop();
  }

  ngOnDestroy() {
    this.cardsSubscription.unsubscribe();
  }

  scrollToTop() {
    const scrollToTop = window.setInterval(() => {
      const pos = window.pageYOffset;
      if (pos > 0) {
        window.scrollTo(0, pos - 500); // how far to scroll on each step
      } else {
        window.clearInterval(scrollToTop);
      }
    }, 16);
  }
}
