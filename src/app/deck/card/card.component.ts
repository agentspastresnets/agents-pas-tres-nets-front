import {Component, Input, OnInit} from '@angular/core';
import {CardService} from '../../services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() cardName: string;
  @Input() cardPicture: string;
  @Input() cardGames: string;
  @Input() cardId: number;
  @Input() index: number;
  @Input() selectedCardId: number;
  @Input() firstJob: string;

  constructor() { }
  ngOnInit() {
  }

}
