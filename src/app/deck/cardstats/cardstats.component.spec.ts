import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardstatsComponent } from './cardstats.component';

describe('CardstatsComponent', () => {
  let component: CardstatsComponent;
  let fixture: ComponentFixture<CardstatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardstatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardstatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
