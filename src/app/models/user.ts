export class User {
  id: number;
  name: string;
  mail: string;
  password: string;
  picture: string;
  role: string;
  title: string;
  friendlist: any[];


  constructor() {
  }
}
