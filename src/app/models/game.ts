export class Game {
  id: number;
  gameStatus: string;
  endDate: Date;
  pauseDate: Date;
  creatorId: number;
  playersId: number[];
  spyId: number;
  placeId: number;
  latitude: number;
  longitude: number;


  constructor() {
  }
}
