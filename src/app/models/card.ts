export class Card {
  id: number;
  name: string;
  picture: string;
  metiers: any[];
  usage: number;

  constructor() {
    this.metiers = this.fillJob();
    this.id = -1;
    this.name = null;
    this.picture = null;
    this.usage = 0;
  }

  fillJob() {
    let metiers = [];
    let i: number;
    for (i = 0; i < 7; i++) {
      metiers.push({ name: null });
    }
    return metiers;
  }
}

