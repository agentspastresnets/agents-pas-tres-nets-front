import {Component, Input, OnInit} from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit {
  title = 'agent-pas-tres-net';
  @Input() isAuth: boolean;

  constructor(private loginService: LoginService) {
    this.isAuth = false;
  }

  ngAfterViewChecked()
  {
    this.isAuth = this.loginService.isAuth;
  }

  ngOnInit() {
  }
}
