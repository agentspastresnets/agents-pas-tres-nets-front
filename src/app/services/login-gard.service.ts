
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { LoginService} from "./login.service";

@Injectable()  //Un service est, par défaut, injectable ailleurs.  Si vous avez besoin des éléments d'un service dans un autre (c'est souvent le cas pour l'authentification ou les communications serveur, par exemple)
export class LoginGuard implements CanActivate {

  constructor(private loginService: LoginService, private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(this.loginService.isAuth) {
      return true;
    } else {
      this.router.navigate(['login']);
    }
  }
}
