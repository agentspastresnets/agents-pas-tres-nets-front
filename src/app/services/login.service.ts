import { Subscription } from 'rxjs';
import {forEach} from "@angular/router/src/utils/collection";
import {User} from '../models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from "@angular/core";
import { Router } from "@angular/router";

@Injectable()
export class LoginService {

  usersurl: string = 'https://aptn.redissi.xyz/api/users/';


  userIdent: User = new User();

  isAuth: boolean = false;
  private httpOptions: Object;

  constructor(private httpClient: HttpClient,
              private router: Router) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };

  }
  signIn(user) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient
          .get <any>(this.usersurl + 'login/' + '?mail=' + user.mail + '&password=' + user.password)
          .subscribe( (response) => {
            this.userIdent = response;
            this.isAuth = true;
            this.router.navigate(['account']);  // une fois loggué redirige vers la page account
            resolve(true);
          },
            (error) => {
              console.log('Erreur ! : bad password ' + error);
          });
      }
    );
  }

  userByID(id) {
    return new Promise(
      (resolve, reject) => {
        this.httpClient
          .get <any>(this.usersurl + id)
          .subscribe( (response) => {
            this.userIdent = response;
            this.isAuth = true;
            this.router.navigate(['account']);  // une fois loggué redirige vers la page account
            resolve(true);
          });
      }
    );
  }

  signUp(user){
    let u = new User();
    u = user;
    this.httpClient
      .post <User>(this.usersurl, u, this.httpOptions)
      .subscribe( (response) => {
        this.userIdent = response;
        this.isAuth = true;
        this.router.navigate(['account']);  // une fois loggué redirige vers la page account
      });
  }

  signOut() {
    this.isAuth = false;
  }

  getUserIdent() {
    return this.userIdent;
  }
}
