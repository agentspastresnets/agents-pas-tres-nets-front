import {Observable, Subject, throwError} from 'rxjs';
import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Card } from '../models/card';
import {catchError, tap} from 'rxjs/operators';

@Injectable()
export class CardService {

  cardsSubject = new Subject<Card[]>();
  newCard: boolean;
  private cards: Card[];
  private httpOptions: object;
  private url: string;

  constructor(private httpClient: HttpClient) {
    this.newCard = true;
    this.cards = [];
    this.url = 'https://aptn.redissi.xyz/api/places/';
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  }

  saveCardToServer(card) {
    if ( card.id === -1 ) { // nouvelle carte, donc on POST sans le ID
      card.id = undefined;
      this.httpClient
        .post(this.url, card, this.httpOptions)
        .subscribe(
          (response) => {
            console.log('Enregistrement terminé !');
            if ( response instanceof Card) {
              card.id = response.id; // redonner le bon ID a la selectedCard
              this.addThisCardInFront(card);
              this.emitCardSubject();
            }
          },
          (error) => {
            console.log('Erreur ! : ' + error);
          });
    } else {  // sinon on PUT avec le bon ID dans l'url
      const id = card.id;
      this.httpClient
        .put(this.url + id, card, this.httpOptions)
        .subscribe(
          () => {
            console.log('Enregistrement terminé !');
            this.getCardsFromServer();
            this.emitCardSubject();
          },
          (error) => {
            console.log('Erreur ! : ' + error);
          });
    }
  }

  addThisCardInFront(card: Card) {
    if ( this.cards.length === 0 && card.id === -1) {
      card.id = 1;
    }
      this.cards.push(card);
  }


  deleteCard(card: Card): Observable<{}> {
    this.cards.map( (c, i) => {
      if ( c.id = card.id ) {
        this.cards.pop();
      }
    });
    return this.httpClient.
    delete(this.url + card.id, this.httpOptions)
      .pipe(
        catchError(this.handleError),
      );
  }


  getCardsFromServer() {
    this.httpClient
      .get <Card[]>(this.url)
      .subscribe(
        (response) => {
          if ( response === null ) {
          } else {
            if (response) {
              this.cards = response;
              if ( this.cards.length > 0 ) {
                this.checkCards(this.cards);
              }
              this.cards.sort((a, b) => b.usage - a.usage);
              this.emitCardSubject();
            }
          }
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }


  checkCards(cards) {
    cards.map((card, i) => {
      if (card.name === undefined) { this.cards[i].name = null; }
      if (card.usage === undefined) { this.cards[i].usage = 0; }
      if (card.picture === undefined) { this.cards[i].picture = null; }
      if (card.id === undefined) { this.cards[i].id = null; }
      if (card.metiers === undefined) { this.cards[i].metiers = []; }
      let j = 0;
      for (j; j < 7; j++) {
        if (this.cards[i].metiers[j] === undefined) {
          this.cards[i].metiers.push({name: null});
        }
      }
    });
    this.emitCardSubject();
  }

  emitCardSubject() {
    this.cardsSubject.next(this.cards.slice());
  }

  getNewCard() {
    const card = new Card();
    if ( this.cards === undefined ) {
      card.id = 1;
      this.cards.push(card);
    } else {
      if ( this.cards.length > 0 ) {
        const id = Math.max.apply(Math, this.cards.map(function (item) {
          return item.id;
        }));
        if (!isFinite(id)) {
          card.id = -1;
        }
        if (card.id !== 1) {
          card.id = -1;
        }
      }
    }
    return card;
   }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}

