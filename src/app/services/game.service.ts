import { Observable, Subject, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Game } from '../models/game';
import { catchError, tap, map } from 'rxjs/operators';
import { User } from '../models/user';


@Injectable()
export class GameService {
  gamesSubject = new Subject<Game[]>();
  currentGamesSubject = new Subject<Game[]>();
  historicalGamesSubject = new Subject<Game[]>();
  users: Map<number, User>;
  private games: Game[];
  private historicalGames: Game[];
  private currentGames: Game[];
  private httpOptions: object;
  private url: string;
  private usersurl: string;

  constructor(private httpClient: HttpClient) {
    this.games = [];
    this.historicalGames = [];
    this.currentGames = [];
    this.users = new Map();
    this.url = 'https://aptn.redissi.xyz/api/games/';
    this.usersurl = 'https://aptn.redissi.xyz/api/users/';
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  }


  getGames() {
    this.httpClient
      .get <Game[]>(this.url)
      .subscribe(
        (response) => {
          if ( response === null ) {
            console.log('Erreur ! aucun jeux');
          } else {
            if (response) {
              this.games = response;
              if ( this.games.length > 0 ) {
                this.games.forEach((g) => {
                  if ( g.gameStatus.includes('WON') ) {
                    this.historicalGames.push(g);
                  } else  {
                    if ( !g.gameStatus.includes('CANCELED')) {
                      this.currentGames.push(g);
                    }
                  }
                });
                // sort the two arrays by dates
                this.historicalGames = this.historicalGames.sort((a: any, b: any) =>
                  new Date(a.endDate).getTime() - new Date(b.endDate).getTime()
                );
                this.currentGames = this.currentGames.sort((a: any, b: any) =>
                  new Date(a.endDate).getTime() - new Date(b.endDate).getTime()
                );

                this.games.forEach((g) => {
                  this.fillUsers(g);
                });
                this.emitGameSubject();
              }
            }
          }
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
  }

  getCurrentGames() {
    return this.httpClient
      .get <Game[]>(this.url)
      .pipe(
        map(games => games.filter((g) => !g.gameStatus.includes('WON') && !g.gameStatus.includes('CANCELED') )),
        map(games => games.sort((a, b) => new Date(b.endDate).getTime() - new Date(a.endDate).getTime()))

      );
  }

  getHistoricalGames() {
    return this.httpClient
      .get <Game[]>(this.url)
      .pipe(
        map(games => games.filter((g) => g.gameStatus.includes('WON'))),
        map(games => games.sort((a, b) => new Date(a.endDate).getTime() - new Date(b.endDate).getTime()))
      );
  }

  fillUsers(game: Game) {
    game.playersId.forEach((u) => {
      if (this.users[u] === undefined) {
        this.httpClient
          .get <any>(this.usersurl + u)
          .subscribe( (response) => {
            const r = response;
            this.users.set(r.id, r);
          });
      }
    });
  }

  getUsers() {
    return this.users;
  }

  emitGameSubject() {
    this.gamesSubject.next(this.games.slice());
    this.currentGamesSubject.next(this.currentGames.slice());
    this.historicalGamesSubject.next(this.historicalGames.slice());
  }
}
