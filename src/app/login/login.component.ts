import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import {User} from '../models/user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  private user: User;
  loginStatus: boolean;
  loginMail: string;
  loginPassword: string;
  signUpPseudo: string;
  loginfail: boolean;
  signUpblock: boolean;
  signUpfail: boolean;

  constructor(private loginService: LoginService, private router: Router) {
    this.loginStatus = null;
    this.loginMail = null;
    this.loginPassword = null;
    this.signUpPseudo = null;

    this.user = new User();
  }

  ngOnInit() {
    this.loginStatus = this.loginService.isAuth;
    if (this.loginStatus === true) {
      this.onSignOut();
    }

    this.loginfail = false;
    this.signUpblock = false;
    this.signUpfail = false;

  }

  onSignIn() {
    this.user.mail = this.loginMail;
    this.user.password = this.loginPassword;
    this.loginService.signIn(this.user).then(
      () => {
      }
    ).catch( err => {
      console.log(err);
      this.loginfail = true;
    });
  }

  onSignUp() {
    this.user.mail = this.loginMail;
    this.user.password = this.loginPassword;
    this.user.name = this.signUpPseudo;
    this.loginService.signUp(this.user);
  }

  onSignOut() {
    this.loginService.signOut();
    this.loginStatus = this.loginService.isAuth;
    this.router.navigate(['home']);  // une fois délogué redirige vers la page home
  }

  onBlokLogin() {
    this.signUpblock = false;
  }

  onBlokSignUp() {
    this.signUpblock = true;
  }

}

